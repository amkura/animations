import { Component } from '@angular/core';
import {animate, group, keyframes, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  animations: [
    trigger('myDivState', [
      state('normal123', style(({
        backgroundColor: 'red',
        transform: 'translateX(0)'
      }))),
      state('highlighted', style({
        backgroundColor: 'blue',
        transform: 'translateX(100px)'
      })),
      transition('normal123 <=> highlighted', animate(300)),
    ]),
    trigger('wildState', [
      state('normal', style(({
        backgroundColor: 'red',
        transform: 'translateX(0)'
      }))),
      state('highlighted', style({
        backgroundColor: 'blue',
        transform: 'translateX(100px)'
      })),
      state('shrunken', style({
        backgroundColor: 'green',
        transform: 'translateX(0px) scale(0.5)'
      })),
      transition('normal => highlighted', animate(300)),
      transition('highlighted => normal', animate(800)),
      // transition('shrunken <=> *', animate(500, style({ // this immediately jumps to the end state
      //   borderRadius: '50px'
      // }))),
      transition('shrunken <=> *', [
        style({ //starting phase if no animate method is used
          backgroundColor: 'orange'
        }),
        animate(1000, style({ // apply a style while animating
          borderRadius: '50px'
        })),
        animate(500) //if we have an animate without style, transition to and end state
      ]),
    ]),
    trigger('list1', [
      state('in', style(({
        opacity: 1,
        transform: 'translateX(0)'
      }))),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100px)'
        }), //applied right in the beginning
        animate(300)
      ]),
      transition('* => *', [
        animate(300, style({
          opacity: 0,
          transform: 'translateX(100px)'
        }))
      ]),
    ]),
    trigger('list2', [
      state('in', style(({
        opacity: 1,
        transform: 'translateX(0)'
      }))),
      transition('void => *', [
        animate(1000, keyframes([ // every style will be triggered in 1/n of a second; overridable by offset
          style({
            transform: 'translateX(-100px)',
            opacity: 0,
            offset: 0
          }),
          style({
            transform: 'translateX(-50px)',
            opacity: 0.5,
            offset: 0.2
          }),
          style({
            transform: 'translateX(-20px)',
            opacity: 1,
            offset: 0.7
          }),
          style({
            transform: 'translateX(0px)',
            opacity: 1,
            offset: 1
          })
        ]))
      ]),
      transition('* => void', [
        group([
          animate(300, style({ // waits for one animation to finish before starting next
            opacity: 0,
            transform: 'translateX(100px)'
          })),
          animate(500, style({
            color: 'red'
          }))
        ])
      ]),
    ]),
  ]
})
export class AppComponent {
  list = ['Milk', 'Sugar', 'Bread'];

  state = 'normal123';
  wildState = 'normal';

  onAnimate() {
    this.state = this.state === 'normal123' ? 'highlighted' : 'normal123';
    this.wildState = this.wildState == 'normal' ? 'highlighted': 'normal';
  }

  onShrink() {
    this.wildState = 'shrunken';
  }

  onDelete(item) {
    this.list.splice(this.list.indexOf(item), 1);
  }

  animationStarted(event) {
    console.log('Start', event);
  }

  animationFinished(event) {
    console.log('End', event);
  }

  onAdd(item) {
    this.list.push(item);
  }
}
